Kubernetes Logo
Quick Start
1.Install dependencies from requirements.txt
sudo pip3 install -r requirements.txt

2. Establish trust between ansible host and all target hosts
ssh-copy-id $target_host

3. Edit
edit inventory/mycluster/hosts.yaml to define target hosts

all:
  hosts:
    node1:
      ansible_host: 192.168.1.106
      ip: 192.168.1.106
    node2:
      ansible_host: 192.168.1.107
      ip: 192.168.1.107
    node3:
      ansible_host: 192.168.1.108
      ip: 192.168.1.108
4. Deploy
navigate to the root folder where we can access "cluster.yml" and execute following command: ansible-playbook -i inventory/mycluster/hosts.yml cluster.yml -b -v --private-key=~/.ssh/private_key

Docker Hub and some library can only be accessible without GFW

5. Uninstall
remove all nodes ansible-playbook -i inventory/mycluster/hosts.yml remove-node.yml -b -v --private-key=~/.ssh/private_key

remove specific nodes ansible-playbook -i inventory/mycluster/hosts.yml remove-node.yml -b -v --private-key=~/.ssh/private_key --extra-vars "node=node1,node2,node3"